import subprocess

# Starting Android emulator
subprocess.call('/Users/dijana/Library/Android/sdk/emulator/emulator -avd Pixel_3_API_29', shell=True)

# Starting Appium server
subprocess.Popen('appium --port 4723 --address 127.0.0.1', shell=True)

# kill adb device
#subprocess.call('adb -s emulator-5554 emu kill', shell=True)
