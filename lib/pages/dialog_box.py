# Imported for wait purposes
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class DialogBox:
    def __init__(self, driver):
        self.driver = driver

    def wait_for_page_to_load(self):
        wait = WebDriverWait(self.driver, 30)
        wait.until(ec.presence_of_element_located((By.ID, 'android:id/alertTitle')))

    def update_available_title(self):
        return self.driver.find_element_by_id('android:id/alertTitle').text

    def update_available_text(self):
        return self.driver.find_elements_by_id('android:id/message')[0].text

    def update_available_button_ignore_label(self):
        return self.driver.find_elements_by_id('android:id/button2')[0].text

    def update_available_button_show_label(self):
        return self.driver.find_elements_by_id('android:id/button1')[0].text

    def update_available_button_ignore_click(self):
        return self.driver.find_elements_by_id('android:id/button2')[0].click()

    def update_available_button_show_click(self):
        return self.driver.find_elements_by_id('android:id/button1')[0].click()

