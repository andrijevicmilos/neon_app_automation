# Imported for wait purposes
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

class UpdatePage:
    def __init__(self, driver):
        self.driver = driver

    def wait_for_page_to_load(self):
        wait = WebDriverWait(self.driver, 30)
        wait.until(ec.presence_of_element_located((By.ID, 'android:id/action_bar_title')))

    def update_title(self):
        return self.driver.find_element_by_id('android:id/action_bar_title').text

    def update_info_app_name(self):
        return self.driver.find_element_by_id('com.neonbanking.bankingapp:id/label_title').text

    def update_info_app_version(self):
        return self.driver.find_element_by_id('com.neonbanking.bankingapp:id/label_version').text

    def update_button_label(self):
        return self.driver.find_element_by_id('com.neonbanking.bankingapp:id/button_update').text

    def update_button_click(self):
        return self.driver.find_element_by_id('com.neonbanking.bankingapp:id/button_update').click()
