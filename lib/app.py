from lib.config.desired_capabilities import *
from lib.pages.dialog_box import *
from lib.pages.landing_page import *
from lib.pages.log_in_page import *
from lib.pages.open_account_page import *
from lib.pages.update_page import *
from lib.pages.dialog_box_partners import *
from lib.pages.about_you_page import *
from appium import webdriver


class App:

    def __init__(self):
        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desired_caps_android_emulator)

        self.dialog_box = DialogBox(self.driver)
        self.landing_page = LandingPage(self.driver)
        self.log_in_page = LogInPage(self.driver)
        self.open_account_page = OpenAccountPage(self.driver)
        self.update_page = UpdatePage(self.driver)
        self.dialog_box_partners = DialogBoxPartners(self.driver)
        self.about_you_page = AboutYouPage(self.driver)
        # And all other pages in format self.<page name> = <class name>(self.driver) 

    def exit(self):
        print("Closing driver")
        self.driver.quit()
