
# DESIRED CAPABILITIES FOR ANDROID **** EMULATOR ****
desired_caps_android_emulator = dict()
desired_caps_android_emulator['platformName'] = 'Android'
desired_caps_android_emulator['platformVersion'] = '10'
desired_caps_android_emulator['deviceName'] = 'emulator-5554'
desired_caps_android_emulator['app'] = '/Users/dijana/PycharmProjects/neon_app/apk_files/whitecase.apk'
desired_caps_android_emulator['autoGrantPermissions'] = 'true'
desired_caps_android_emulator['autoAcceptAlerts'] = 'true'
desired_caps_android_emulator['unicodeKeyboard'] = 'false'
desired_caps_android_emulator['automationName'] = 'UIautomator2'


# DESIRED CAPABILITIES FOR ANDROID **** REAL DEVICE ****
desired_caps_android_real_device = dict()
desired_caps_android_real_device['platformName'] = 'Android'
desired_caps_android_real_device['platformVersion'] = '9'
desired_caps_android_real_device['deviceName'] = 'ce0718276121522b01'
desired_caps_android_real_device['app'] = '/Users/dijana/PycharmProjects/neon_app/apk_files/whitecase.apk'
desired_caps_android_real_device['autoGrantPermissions'] = 'true'
desired_caps_android_real_device['autoAcceptAlerts'] = 'true'
desired_caps_android_real_device['unicodeKeyboard'] = 'false'
