class DialogBoxStrings:
    title = "Update Available"
    text = "Show information about the new update?"
    button_ignore = "IGNORE"
    button_show = "SHOW"


class LandingPageStrings:
    button_login = "Log in"
    button_open_account = "Open account"
    screen1_title = "neon\nfeel free"
    screen1_text = "Not yet with neon? Open your account in less than 10 minutes – no fees, no paperwork."
    screen2_title = "neon everywhere, all the time"
    screen2_text = "Check your balance or pay a bill while you wait for the bus – neon is always at your fingertips."
    screen3_title = "One card for everything"
    screen3_text = "Pay less when you withdraw money at any ATM, shop online or in stores worldwide - with your free neon card."
    screen4_title = "Full control over your money"
    screen4_text = "Remain always in control of your money thanks to your personal spending analysis."
    screen5_title = "Guaranteed safe"
    screen5_text = "Your account is insured up to 100'000 CHF via Hypothekarbank Lenzburg and rigourously protected – totally worry-free."


class UpdatePageStrings:
    title = "App Update"
    info_app_name = "neon"
    info_app_version = "Version 0.0.3 (4845)\n27.11.2019 - 34.36 MB"
    button_update = "Update"


class LogInPageStrings:
    placeholder_text = "Email or contact number"
    button_continue = "CONTINUE"


class OpenAccountPageStrings:
    title = "In 3 steps\nto your\nneon account"
    step1_title = "About you Step 1"
    step1_text = "We need some information from you."
    step2_title = "Identity check Step 2"
    step2_text = "We briefly check your identity. Have your passport or ID ready."
    step3_title = "First Login Step 3"
    step3_text = "Enter your login information as soon as you get it."
    button_lets_go = "LET'S GO!"


class DialogBoxPartnersStrings:
    title = "We treat your data with respect. "
    text = "For your banking experience we are working with the Hypothekarbank Lenzburg (HBL). For your identification via video call, Intrum/IDnow are supporting us. Our identification partners will delete your data after 90 days."
    button_alright_lets_go = "ALRIGHT, LET'S GO!"

class AboutYouStrings:
    title = "About youPERSONAL INFORMATION"
    first_name = "FIRST NAME"
    last_name = "LAST NAME"
    email = "EMAIL"
    gender_title = "GENDER"
    gender_male = "Male"
    gender_female = "Female"
    gender_other = "Other"
    date_of_birth = "DATE OF BIRTH"
    place_of_origin = "PLACE OF ORIGIN"
    civil_status = "CIVIL STATUS"
    civil_status_single ="Single"
    civil_status_registered_partnership = "Registered partnership"
    civil_status_married = "Married"
    civil_status_divorced = "Divorced"
    civil_status_widowed = "Widowed"
    nationality = "NATIONALITY"
    button_continue = "CONTINUE arrow forward-outline"
    button_back = "BACK"
    cancel = "CANCEL"
    finished = "FINISHED"
    correct = "Correct?"
    continue_to_your_contact_details = "Continue to your contact details."
    confirm_button_label = "YES, TRUE! arrow forward-outline"