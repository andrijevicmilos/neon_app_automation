import unittest
from lib.app import App
from lib.config.strings import *


class TestLoginFlow(unittest.TestCase):

    def setUp(self):

        self.instance1 = App()
        self.instance1.dialog_box.wait_for_page_to_load()

    def tearDown(self):
        self.instance1.exit()

    def test_log_in_successful(self):
        self.instance1.dialog_box.update_available_button_ignore_click()
        self.instance1.landing_page.button_login_click()
        self.instance1.log_in_page.wait_for_username_page_to_load()
        self.assertEqual(LogInPageStrings.button_continue, self.instance1.log_in_page.button_continue_label())
        self.assertEqual(False, self.instance1.log_in_page.button_continue_status())
        self.instance1.log_in_page.login_field_enter_name("123456")
        self.assertEqual(True, self.instance1.log_in_page.button_continue_status())
        self.instance1.log_in_page.button_continue_click()



