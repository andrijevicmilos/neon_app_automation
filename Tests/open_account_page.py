import unittest
from lib.app import App
from lib.config.strings import *
from time import sleep


class TestOpenAccountFlow(unittest.TestCase):

    def setUp(self):

        self.instance1 = App()
        self.instance1.dialog_box.wait_for_page_to_load()

    def tearDown(self):
        self.instance1.exit()

    def test_open_account(self):
        self.instance1.dialog_box.update_available_button_ignore_click()
        self.instance1.landing_page.button_open_account_click()
        self.instance1.open_account_page.wait_for_page_to_load()
        self.assertEqual(OpenAccountPageStrings.title, self.instance1.open_account_page.title())
        self.assertEqual(OpenAccountPageStrings.step1_title, self.instance1.open_account_page.step1_title())
        self.assertEqual(OpenAccountPageStrings.step1_text, self.instance1.open_account_page.step1_text())
        self.assertEqual(OpenAccountPageStrings.step2_title, self.instance1.open_account_page.step2_title())
        self.assertEqual(OpenAccountPageStrings.step2_text, self.instance1.open_account_page.step2_text())
        self.assertEqual(OpenAccountPageStrings.step3_title, self.instance1.open_account_page.step3_title())
        self.assertEqual(OpenAccountPageStrings.step3_text, self.instance1.open_account_page.step3_text())
        self.assertEqual(OpenAccountPageStrings.button_lets_go, self.instance1.open_account_page.button_lets_go_label())
        self.instance1.open_account_page.button_lets_go_click()
        # PARTNERS DIALOG BOX
        self.instance1.dialog_box_partners.wait_for_page_to_load()
        self.assertEqual(DialogBoxPartnersStrings.title, self.instance1.dialog_box_partners.title())
        self.assertEqual(DialogBoxPartnersStrings.text, self.instance1.dialog_box_partners.text())
        self.assertEqual(DialogBoxPartnersStrings.button_alright_lets_go, self.instance1.dialog_box_partners.button_alright_lets_go_label())
        self.instance1.dialog_box_partners.button_alright_lets_go_click()
        # FIRST NAME FIELD
        self.instance1.about_you_page.wait_for_page_to_load()
        self.instance1.about_you_page.field_first_name_click()
        self.assertEqual(AboutYouStrings.first_name, self.instance1.about_you_page.field_first_name_label())
        self.instance1.about_you_page.field_first_name_input()
        # LAST NAME FIELD
        self.instance1.about_you_page.field_last_name_click()
        self.assertEqual(AboutYouStrings.last_name, self.instance1.about_you_page.field_last_name_label())
        self.instance1.about_you_page.field_last_name_input()
        # EMAIL FIELD
        self.instance1.about_you_page.field_email_click()
        self.assertEqual(AboutYouStrings.email, self.instance1.about_you_page.field_email_input_label())
        self.instance1.about_you_page.field_email_input()
        # GENDER FIELD
        self.instance1.about_you_page.field_gender_click()
        self.assertEqual(AboutYouStrings.gender_title, self.instance1.about_you_page.field_gender_radio_button_title())
        self.assertEqual(AboutYouStrings.gender_male, self.instance1.about_you_page.field_gender_radio_button_male_label())
        self.assertEqual(AboutYouStrings.gender_female, self.instance1.about_you_page.field_gender_radio_button_female_label())
        self.assertEqual(AboutYouStrings.gender_other, self.instance1.about_you_page.field_gender_radio_button_other_label())
        self.instance1.about_you_page.field_gender_radio_button_male_click()
        # DATE OF BIRTH
        self.instance1.about_you_page.field_date_of_birth_click()
        self.instance1.about_you_page.wait_for_birthday_scroll_to_load()
        self.instance1.about_you_page.field_date_of_birth_select_date()
        self.assertEqual(AboutYouStrings.cancel, self.instance1.about_you_page.field_date_of_birth_button_cancel_label())
        self.assertEqual(AboutYouStrings.finished, self.instance1.about_you_page.field_date_of_birth_button_finished_label())
        self.instance1.about_you_page.field_date_of_birth_button_finished_click()
        # PLACE OF ORIGIN
        self.instance1.about_you_page.field_place_of_origin_click()
        self.instance1.about_you_page.wait_for_field_place_of_origin_label()
        self.assertEqual(AboutYouStrings.place_of_origin, self.instance1.about_you_page.field_place_of_origin_label())
        self.instance1.about_you_page.field_place_of_origin_input()
        self.assertEqual(AboutYouStrings.button_continue, self.instance1.about_you_page.button_continue_label())
        self.assertEqual(AboutYouStrings.button_back, self.instance1.about_you_page.button_back_label())
        self.instance1.about_you_page.button_continue_click()
        # CIVIL STATUS
        self.instance1.about_you_page.wait_for_field_civil_status_title()
        self.assertEqual(AboutYouStrings.civil_status, self.instance1.about_you_page.field_civil_status_title())
        self.assertEqual(AboutYouStrings.civil_status_single, self.instance1.about_you_page.field_civil_status_single_label())
        self.assertEqual(AboutYouStrings.civil_status_registered_partnership, self.instance1.about_you_page.field_civil_status_registered_partnership_label())
        self.assertEqual(AboutYouStrings.civil_status_married, self.instance1.about_you_page.field_civil_status_married_label())
        self.assertEqual(AboutYouStrings.civil_status_divorced, self.instance1.about_you_page.field_civil_status_divorced_label())
        self.assertEqual(AboutYouStrings.civil_status_widowed, self.instance1.about_you_page.field_civil_status_widowed_label())
        self.instance1.about_you_page.field_civil_status_married_click()
        # NATIONALITY
        self.instance1.about_you_page.field_nationality_click()
        self.instance1.about_you_page.wait_for_field_nationality_label()
        self.assertEqual(AboutYouStrings.nationality, self.instance1.about_you_page.field_nationality_label())
        self.instance1.about_you_page.field_nationality_picker()
        self.instance1.about_you_page.wait_for_field_nationality_button_finished_label()
        self.assertEqual(AboutYouStrings.finished, self.instance1.about_you_page.field_nationality_button_finished_label())
        self.assertEqual(AboutYouStrings.cancel, self.instance1.about_you_page.field_nationality_button_cancel_label())
        self.instance1.about_you_page.field_nationality_button_finished_click()
        self.instance1.about_you_page.button_continue_click()
        self.assertEqual(AboutYouStrings.confirm_button_label, self.instance1.about_you_page.confirm_button_label())
        self.instance1.about_you_page.confirm_button_click()
        sleep(5)




