import unittest
from lib.app import App
from lib.config.strings import *
from time import sleep
from selenium.common.exceptions import NoSuchElementException
import subprocess
from lib.config.desired_capabilities import *
from appium import webdriver


class LandingPage(unittest.TestCase):

    def setUp(self):
        self.instance1 = App()
        self.instance1.dialog_box.wait_for_page_to_load()

    def tearDown(self):
        self.instance1.exit()

    def test_landing_page_swipe(self):
        # Closing update available dialog box
        self.instance1.dialog_box.update_available_button_ignore_click()
        # Checking landing page 1 content
        self.assertEqual(LandingPageStrings.screen1_title, self.instance1.landing_page.title())
        self.assertEqual(LandingPageStrings.screen1_text, self.instance1.landing_page.text())
        self.assertEqual(LandingPageStrings.button_login, self.instance1.landing_page.button_login_label())
        self.assertEqual(LandingPageStrings.button_open_account, self.instance1.landing_page.button_open_account_label())
        self.instance1.landing_page.button_navigation_dots_2_click()
        sleep(2)
        # Checking landing page 2 content
        self.assertEqual(LandingPageStrings.screen2_title, self.instance1.landing_page.title())
        self.assertEqual(LandingPageStrings.screen2_text, self.instance1.landing_page.text())
        self.assertEqual(LandingPageStrings.button_login, self.instance1.landing_page.button_login_label())
        self.assertEqual(LandingPageStrings.button_open_account, self.instance1.landing_page.button_open_account_label())
        self.instance1.landing_page.button_navigation_dots_3_click()
        sleep(2)
        # Checking landing page 3 content
        self.assertEqual(LandingPageStrings.screen3_title, self.instance1.landing_page.title())
        self.assertEqual(LandingPageStrings.screen3_text, self.instance1.landing_page.text())
        self.assertEqual(LandingPageStrings.button_login, self.instance1.landing_page.button_login_label())
        self.assertEqual(LandingPageStrings.button_open_account, self.instance1.landing_page.button_open_account_label())
        self.instance1.landing_page.button_navigation_dots_4_click()
        sleep(2)
        # Checking landing page 4 content
        self.assertEqual(LandingPageStrings.screen4_title, self.instance1.landing_page.title())
        self.assertEqual(LandingPageStrings.screen4_text, self.instance1.landing_page.text())
        self.assertEqual(LandingPageStrings.button_login, self.instance1.landing_page.button_login_label())
        self.assertEqual(LandingPageStrings.button_open_account, self.instance1.landing_page.button_open_account_label())
        self.instance1.landing_page.button_navigation_dots_5_click()
        sleep(2)
        # Checking landing page 5 content
        self.assertEqual(LandingPageStrings.screen5_title, self.instance1.landing_page.title())
        self.assertEqual(LandingPageStrings.screen5_text, self.instance1.landing_page.text())
        self.assertEqual(LandingPageStrings.button_login, self.instance1.landing_page.button_login_label())
        self.assertEqual(LandingPageStrings.button_open_account, self.instance1.landing_page.button_open_account_label())



